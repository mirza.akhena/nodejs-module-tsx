FROM node:lts-alpine AS builder

WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY tsconfig.json ./
COPY src src
RUN npm run build

FROM node:lts-alpine
WORKDIR /app
RUN chown node:node .
USER node
COPY package*.json ./
RUN npm ci
COPY --from=builder /app/dist/ dist/

CMD ["npm", "start"]